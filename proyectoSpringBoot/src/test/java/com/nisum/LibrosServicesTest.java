package com.nisum;

import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.when;

import java.util.ArrayList;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.nisum.dao.LibrosDao;
import com.nisum.model.Libro;
import com.nisum.service.LibrosService;

import junit.framework.Assert;

@RunWith(MockitoJUnitRunner.class)
public class LibrosServicesTest {
	
	@Mock
	private LibrosDao librosDao;
	
	@InjectMocks
	private LibrosService librosService;
	
	@Test
	public void deboCrearUnLibroYRetornarlo(){
		//arrange
		Libro libro = new Libro();
		//act
		when(librosDao.save(libro)).thenReturn(libro);
		Libro libroCreado = librosService.crearLibro(libro);
		//assert
		Assert.assertNotNull(libroCreado);
		Assert.assertEquals(libro, libroCreado);
		
	}
	
	@Test 
	public void obtenerUnLibroAtravesDeSuId(){
		
		//arrange
		Libro libro = new Libro();
		//act
		when(librosDao.findOne((long) 1)).thenReturn(libro);
		Libro libroCreado = librosService.retornarLibro((long)1);
		//assert
		Assert.assertNotNull(libroCreado);
		Assert.assertEquals(libro, libroCreado);
	}
	
	@Test
	public void obtenerTodosLosLibros(){
		
		//arrange
		ArrayList<Libro> libro = new ArrayList<Libro>();
		//act
		when(librosDao.findAll()).thenReturn(libro);
		ArrayList<Libro> libroCreado = librosService.retornarTodos();
		//assert
		Assert.assertNotNull(libroCreado);
		Assert.assertEquals(libro, libroCreado);
	}
	

}
