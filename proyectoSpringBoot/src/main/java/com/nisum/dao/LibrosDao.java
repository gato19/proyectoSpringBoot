package com.nisum.dao;

import org.springframework.data.repository.CrudRepository;

import com.nisum.model.Libro;
//para acceso de datos
public interface LibrosDao extends CrudRepository<Libro,Long>{

}
