package com.nisum.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;

import com.nisum.dao.LibrosDao;
import com.nisum.model.Libro;

public class LibrosService {
	
	private final LibrosDao libroDao;
	
	//inyectar clase
	@Autowired
	public LibrosService(LibrosDao libroDao){
		this.libroDao = libroDao;
	}
	
	//@Autowired
	//private LibrosDao libroDao;
	
	public Libro crearLibro(Libro libro){
		return libroDao.save(libro);
		
	}
	
	public Libro retornarLibro(Long i){
		return libroDao.findOne(i);
		
	}
	
	public ArrayList<Libro> retornarTodos(){
		return (ArrayList<Libro>) libroDao.findAll();
	}
	
	//obtener un libro a travez de un id, le pasamos un id y nos retorna un libro asociado a ese libro metodo FindOne
    //Obtener todos los libros, utilizar arrayList
}
